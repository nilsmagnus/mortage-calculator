package no.nils.mortage

import grails.converters.JSON

class CalculatorController {

    static double LOAN_STEP = 50000

    def index() {
        render ""
    }

    def calculate() {
        println params

        def years = params.int('years')

        def loan = params.int('loan')

        def interest = params.double('interest')


        def results = []
        def interests = []
        def loans = []

        (-5..5).each {loanIteration ->
            def currentLoan = loan + loanIteration * LOAN_STEP
            loans[loanIteration + 5] = currentLoan
            results[loanIteration + 5] = []
            (-5..5).each { interestIteration ->
                def currentInterest = (interest + 0.5 * interestIteration)
                def currentPayment = calculateTerm(currentLoan.toInteger(), years, currentInterest)
                interests[interestIteration + 5] = (float) currentInterest
                results[loanIteration + 5][interestIteration + 5] = currentPayment
            }
        }

        def toRender = [result: results, interest: interests, loan: loans]

        render toRender as JSON
    }

    def calculateTerm(Integer loan, Integer years, Double interest) {
        interest /= 100

        def interestPower = (1 + interest) ** years;

        def monthlyPayment = loan * interest * (interestPower) / (interestPower - 1)

        (int)(monthlyPayment/12)
    }
}
