<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes" />
    <title><g:layoutTitle default="Grails"/></title>
    <r:require module="bootstrap"/>
    <r:require module="jquery"/>
    <g:layoutHead/>
    <r:layoutResources/>
</head>

<body>

<div class="container">

    <g:layoutBody/>
    <div class="footer" role="contentinfo"></div>

</div>
<g:javascript library="application"/>
<r:layoutResources/>
</body>
</html>
