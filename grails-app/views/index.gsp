<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
    <r:require module="jquery"></r:require>
</head>

<body>

<script type="text/javascript">

    function colorize() {
        for (var i = 0; i < 10; i++) {
            for (var j = 0; j < 10; j++) {
                var key = "#r" + i + "c" + j;
                var val = $(key).text() * 1;
            }
        }
    }

    function showResult(data) {
        for (var i = 0; i < data.interest.length; i++) {
            var key = "#head" + i
            $(key).text(data.interest[i] + "%");
        }

        for (var i = 0; i < data.loan.length; i++) {
            var key = "#row" + i
            $(key).text(data.loan[i]);
        }

        var payments = data.result

        for (var i = 0; i < payments.length; i++) {
            for (var j = 0; j < payments[0].length; j++) {
                var key = "#r" + i + "c" + j;
                $(key).text(payments[i][j]);
            }
        }
        colorize();
    }

    function calculate() {
        var interest = $("#interest").val();
        var loan = $("#loan").val();
        var years = $("#years").val();
        var data = {interest:interest, loan:loan, years:years};

        $.ajax({
            url:"calculator/calculate",
            data:data,
            dataType:"json",
            success:showResult
        });
    }

    function calculateThreshold() {
        var income = $("#income").val();
        var expenses = $("#expenses").val();

        if (income == null || income < 1 || income == "") {
            income = 0;
        }

        if (expenses == null || expenses < 1 || expenses == "") {
            expenses = 0;
        }

        $("#threshold").text("Mortage threshold: " + (income - expenses));

        colorize(income - expenses);

    }

    $(document).ready(function () {
        $("#calculate-btn").click(calculate);
        $("#income").change(calculateThreshold);
        $("#expenses").change(calculateThreshold);
    });
</script>

<div class="row">
    <div class="page-header">
        <h1>Mortage calculator<small> [annuity loan]</small></h1>
    </div>
</div>

<div class="row-fluid">

    <div id="mortage-inputs" class="span3">
        <h4 class="text-info">Mortage</h4>

        <label for="loan">Mortage</label>
        <input class="input-block-level" type="number" step="1" id="loan" placeholder="Amount"
               required="true">
        <label for="loan">Interest(e.g. "4.21")</label>
        <input class="input-block-level" type="number" step="0.01" id="interest"
               placeholder="Interest(%)">
        <label for="loan">Downpayment</label>
        <input class="input-block-level" type="number" step="1" id="years" placeholder="Years">

        <div class="btn btn-primary" id="calculate-btn">Calculate</div>
    </div>

    <div id="income-inputs" class="span3">
        <h4 class="text-info">Income</h4>

        <label for="income">Total monthly income after taxes</label>
        <input class="input-block-level" type="number" id="income" placeholder="Total income">
        <label for="income">Total monthly expenses after taxes</label>
        <input class="input-block-level" type="number" id="expenses" placeholder="Total expenses">

        <h4 class="muted pull-right"><span id="threshold"></span>
        </h4>

    </div>
</div>


<div class="row-fluid">
    <div class="span10">
        <table class="table table-striped table-hover table-bordered">
            <thead>
            <tr>
                <th>Amount\Interest</th>
                <g:each in="${0..10}" var="head">
                    <th><div id="head${head}">-%</div></th>
                </g:each>
            </tr>
            </thead>

            <g:each in="${0..10}" var="row">

                <tr
                    <g:if test="${row == 5}">
                        class="warning"
                    </g:if>>
                    <td><div id="row${row}">-</div></td>

                    <g:each in="${0..10}" var="col">
                        <td><div id="r${row}c${col}">-</div></td>
                    </g:each>

                </tr>
            </g:each>
        </table>
    </div>

</div>

</body>
</html>
